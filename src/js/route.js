var app = angular.module("myApp", ["ngRoute", "chart.js"]);
app.config(function($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "view/home.htm"
    })
    //customer
    .when("/customers", {
      templateUrl: "view/customers/all-customers.htm"
    })
    .when("/customers/registration", {
      templateUrl: "view/customers/registration.htm"
    })
    .when("/customers/registration/success", {
      templateUrl: "view/customers/success.html"
    })
    .when("/customers/info", {
      templateUrl: "view/customers/info.html"
    })
    .when("/customers/edit", {
      templateUrl: "view/customers/edit.htm"
    })
    //Subscription
    .when("/subscription/add", {
      templateUrl: "view/subscription/add-subscription.html"
    })
    .when("/subscription/info", {
      templateUrl: "view/subscription/subscription-detail.htm"
    })
    //tenant template
    .when("/tenant-template", {
      templateUrl: "view/tenant/all-tenants.htm"
    })
    .when("/tenant-template/add", {
      templateUrl: "view/tenant/registration.htm"
    })
    .when("/tenant-template/info", {
      templateUrl: "view/tenant/detail.htm"
    })
    //timezone
    .when("/timezone", {
      templateUrl: "view/timezone/all-timezone.htm"
    })
    .when("/timezone/add", {
      templateUrl: "view/timezone/registration.htm"
    })
    .when("/timezone/edit", {
      templateUrl: "view/timezone/edit.htm"
    })
    //application
    .when("/application", {
      templateUrl: "view/application/all-applications.htm"
    })
    .when("/application/info", {
      templateUrl: "view/application/applications-detail.htm"
    })
    .when("/application/add", {
      templateUrl: "view/application/registration.htm"
    })
    //contact person
    .when("/contact/add", {
      templateUrl: "view/contact_person/registration.htm"
    })

    //past trend
    .when("/past_trend/past_trend_account", {
      templateUrl: "view/past_trend/past_trend_account.html"
    })

    .when("/past_trend/past-trend-transaksi", {
      templateUrl: "view/past_trend/past-trend-transaksi.html"
    })
    .when("/past_trend/past_trend_status", {
      templateUrl: "view/past_trend/past_trend_status.html"
    })

    //default
    .when("/404", {
      templateUrl: "view/default/404.htm"
    })
    .otherwise({ redirectTo: "/404" });
});
