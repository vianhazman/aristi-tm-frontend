app.controller('listAppCtrls', function($scope, $http, $location) {
	console.log('aksjdhakjsdhakjsh');

	$scope.listTypes = [];

	$scope.listUsageParameters = [];

	$scope.listPackages = [];

	$scope.listApps = [];
	//GET Customer
	var customerId = $location.search().id;
	
	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/customer/' + customerId,
		
	}).then(function mySuccess(response) {
		$scope.customerInfo = response.data;
	}, function myError(reason) {
		$scope.customerInfo = reason.statusText;
	});
	//GET Application
	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		dataType : 'json',
		url : 'http://localhost:8080/tenancy-management/api/application/list'
	}).then(function mySuccess(response) {
		$scope.listApps = response.data;
		console.log($scope.listApps);
	}, function myError(reason) {
		$scope.listApps = reason.statusText;
	});
	$scope.setListTypes = function() { 
		
		var idToGet = $scope.form.appVal;
		//GET type
		$http({
			headers : {
				'Authorization' : 'Basic cmVzdDpyZXN0',
			},
			method : 'GET',
			url : 'http://localhost:8080/tenancy-management/api/template/list',
			params : {
				appId : idToGet
			},
			dataType : 'json'
		}).then(function mySuccess(response) {
			$scope.listTypes = response.data;
		}, function myError(reason) {
			$scope.listTypes = reason.statusText;
		});
		//Get Package
		$http({
			headers : {
				'Authorization' : 'Basic cmVzdDpyZXN0',
			},
			method : 'GET',
			url : 'http://localhost:8080/tenancy-management/api/usage-parameter/list',
			params : {
				appId : idToGet
			},
			dataType : 'json'
		}).then(function mySuccess(response) {
			$scope.listUsageParameters = response.data;
		}, function myError(reason) {
			$scope.listUsageParameters = reason.statusText;
		});
		//GET Usage Limit
		$http({
			headers : {
				'Authorization' : 'Basic cmVzdDpyZXN0',
			},
			method : 'GET',
			url : 'http://localhost:8080/tenancy-management/api/app-package/list',
			params : {
				appId : idToGet
			},
			dataType : 'json'
		}).then(function mySuccess(response) {
			$scope.listPackages = response.data;
		}, function myError(reason) {
			$scope.listPackages = reason.statusText;
		});
	};
	$scope.validation = function(){
		$scope.isAppInvalid="";
		$scope.isTypeInvalid="";
		$scope.isPackageInvalid=""; 
		$scope.isIPInvalid="";
		$scope.isPortInvalid=""; 
		$scope.isUserInvalid="";
		$scope.isPassInvalid="";
		$scope.isPDDInvalid="";
		$scope.isPaymentInvalid=""; 
		$scope.isDateInvalid = "";
		$scope.isLengthInvalid = "";

		if($scope.form.appVal==null){
			$scope.isAppInvalid = "is-invalid";
			
		}else if ($scope.form.typeVal==null) {
			$scope.isTypeInvalid = "is-invalid";
		}else if ($scope.form.typePackage==null) {
			$scope.isPackageInvalid = "is-invalid";
				
		}else if ($scope.form.inputIp==null) {
			$scope.isIPInvalid = "is-invalid";
					
		}else if ($scope.form.inputPort==null) {
			$scope.isPortInvalid = "is-invalid";
		}else if ($scope.form.inputUsername==null) {
			$scope.isUserInvalid = "is-invalid";
		}else if($scope.form.inputPassword==null){
			$scope.isPassInvalid = "is-invalid";
		}else if ($scope.form.paymentDueDate==null) {
			$scope.isPDDInvalid = "is-invalid";
		}else if ($scope.form.paymentPeriod==null) {
			$scope.isPaymentInvalid = "is-invalid";
		}else if ($scope.form.contractDateStart==null) {
			$scope.isDateInvalid = "is-invalid";
		}else if ($scope.form.contractValidFor==null) {
			$scope.isLengthInvalid = "is-invalid";
		}
		else {
			$scope.submitFunction();
		}
	};
	$scope.submitFunction = function() {
		
		// var oneOfTheIds1 = angular.element('#up1').val();
		// var oneOfTheIds2 = angular.element('#up2').val();
		// var oneOfTheIds3 = angular.element('#up3').val();
		// var oneOfTheIds4 = angular.element('#up4').val();

		// console.log("get value of up1= " + oneOfTheIds1);
		// console.log("get value of up1= " + oneOfTheIds2);
		// console.log("get value of up1= " + oneOfTheIds3);
		// console.log("get value of up1= " + oneOfTheIds4);

		var application = $scope.form.appVal;
		var type = $scope.form.typeVal;
		var ip = $scope.form.inputIp;
		var port = $scope.form.inputPort;
		var username = $scope.form.inputUsername;
		var password = $scope.form.inputPassword;
		var appPackageId = $scope.form.typePackage;
		// window.alert(customerId);
		var paymentDueDate = $scope.form.paymentDueDate;
		var paymentPeriod = $scope.form.paymentPeriod;
		var contractDateStart = $scope.form.contractDateStart;
		var contractValidFor = $scope.form.contractValidFor;

		var tenantConnection = {};
		tenantConnection.id = application;
		tenantConnection.schemaServer = ip;
		tenantConnection.schemaServerPort = port;
		tenantConnection.schemaUsername = username;
		tenantConnection.schemaPassword = password;
		tenantConnection.schemaName = type;

		var tenants = {};
		tenants.name = customerId;
		tenants.identifier = type;
		tenants.timezoneId = application;

		var subscription = {};
		subscription.customerId = customerId;
		subscription.tenantTypeId = type;
		subscription.appId = application;
		subscription.appPackageId = appPackageId;
		subscription.paymentDueDate = paymentDueDate;
		subscription.paymentPeriodDuration = paymentPeriod;
		subscription.contractDateStart = contractDateStart;
		subscription.id = contractValidFor;
		// window.alert(tenantConnection);

		$http({
			headers : {
				'Authorization' : 'Basic cmVzdDpyZXN0',
			},
			method : 'POST',
			url : 'http://localhost:8080/tenancy-management/api/connection/add/newtenant',
			dataType : 'json',
			data : tenantConnection
		}).then(function mySuccess(response) {
			// window.alert("TenantConnection success");
			console.log(response.data);
			createTenant();
		}, function myError(reason) {
			window.alert(reason.statusText);
		});

		function createTenant() {
			$http({
				headers : {
					'Authorization' : 'Basic cmVzdDpyZXN0',
				},
				method : 'POST',
				url : 'http://localhost:8080/tenancy-management/api/tenant/add/newtenant',
				dataType : 'json',
				data : tenants
			}).then(function mySuccess(response) {
				console.log("success");
				console.log(response.data);
				createSubscription();
			}, function myError(reason) {
				console.log(reason.statusText);
			});
		}

		function createSubscription() {
			$http({
				headers : {
					'Authorization' : 'Basic cmVzdDpyZXN0',
				},
				method : 'POST',
				url : 'http://localhost:8080/tenancy-management/api/subscription/add/newtenant',
				dataType : 'json',
				data : subscription
			}).then(function mySuccess(response) {
				console.log("success");
				console.log(response.data);
			}, function myError(reason) {
				console.log(reason.statusText);
			});
		}
	}
});
app.controller('subscriptionInfoCtrl', function($scope, $http,$location) {

	var subId = $location.search().subId; 
	var cId = $location.search().cid; 

	var appId ="";
	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/customer/' + cId,
		
	}).then(function mySuccess(response) {
		$scope.customerInfo = response.data;
	}, function myError(reason) {
		$scope.customerInfo = reason.statusText;
	});
	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/subscription/detail?subscriptionId=' + subId,
		
	}).then(function mySuccess(response) {
		$scope.subscriptionInfo = response.data;
	}, function myError(reason) {
		$scope.subscriptionInfo = reason.statusText;
	});

	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
				 },
			method : 'GET',
			datatype : 'json',
			url : 'http://localhost:8080/tenancy-management/api/usage/list?subscriptionId=' + subId,		
			}).then(function mySuccess(response) {
			$scope.usageinfo = response.data;
			console.log($scope.appinfo);
			}, function myError(reason) {
			$scope.usageinfo = reason.statusText;					
		});

	
});