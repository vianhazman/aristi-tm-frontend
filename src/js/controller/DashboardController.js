// Controller Chart Line Series
app.controller("TestCtrl", function($scope, $http) {
  //Inisialisasi Data
  $scope.selectedValue = "tenant_test";
  $scope.remarksList = [];
  changeData();
  BindYears();
  //Mengganti Perubahan Pada Dropdown
  $scope.$watch("selectedValue", function() {
    changeData();
  });

  //Array nama tenant yang tersedia
  function BindYears() {
    $scope.YearList = ["tenant_test"];
  }

  //ID Chart pada HTML
  // Dokumentasi; https://plot.ly/javascript/
  var target_test = document.getElementById("test");
  var target_test2 = document.getElementById("test2");
  var layout = {
    xaxis: {
      title: "Transaction Date"
    },

    yaxis: {
      title: "Amount"
    }
  };

  $scope.loading = true;
  function changeData() {
    var data = [];
    console.log("change data");

    //Fetch data dari backend
    $http({
      headers: {
        Authorization: "Basic cmVzdDpyZXN0"
      },
      method: "GET",
      datatype: "json",
      params: { name: $scope.selectedValue },
      url:
        "http://propensi.cico.me:8080/tenancy-management/api/dashboard/transaksi"
    }).then(
      function mySuccess(response) {
        //Memasukkan tiap series ke dalam chart
        response.data.data.forEach(function(element) {
          var plot = {
            showlegend: true,
            mode: "lines",
            type: "scatter",
            name: element.name
          };
          plot.y = element.values;
          var dateString = response.data.date.map(function(date) {
            return date.year + "-" + date.month + "-" + date.day;
          });
          plot.x = dateString;
          data.push(plot);
          $scope.remarksList.push(element.name);
        });
        console.log($scope.remarksList);
        $scope.loading = false;
        //Inisialisasi plot
        Plotly.newPlot(target_test, data, layout, { responsive: true });
        var a = [];
        a.push(data[1]);
        Plotly.newPlot(target_test2, a, layout, {
          responsive: true
        });
      },
      function myError(reason) {}
    );
  }
});

// Controller Chart Line Series
app.controller("AllTransactionCtrl", function($scope, $http) {
  //Inisialisasi Data
  $scope.selectedValue = "";
  changeData();
  BindYears();
  //Mengganti Perubahan Pada Dropdown
  $scope.$watch("selectedValue", function() {
    changeData();
  });

  //Array nama tenant yang tersedia
  function BindYears() {
    $scope.YearList = ["tenant_test"];
  }

  //ID Chart pada HTML
  // Dokumentasi; https://plot.ly/javascript/
  var target_test = document.getElementById("transaction");
  var layout = {
    xaxis: {
      title: "Transaction Date"
    },

    yaxis: {
      title: "Amount"
    }
  };

  $scope.loading = true;
  function changeData() {
    var data = [];
    console.log("change data");

    //Fetch data dari backend
    $http({
      headers: {
        Authorization: "Basic cmVzdDpyZXN0"
      },
      method: "GET",
      datatype: "json",
      url:
        "http://propensi.cico.me:8080/tenancy-management/api/dashboard/transaksi?name=" +
        $scope.selectedValue
    }).then(
      function mySuccess(response) {
        //Memasukkan tiap series ke dalam chart
        response.data.data.forEach(function(element) {
          var plot = {
            showlegend: true,
            mode: "lines",
            type: "scatter",
            name: element.name
          };
          plot.y = element.values;
          var dateString = response.data.date.map(function(date) {
            return date.year + "-" + date.month + "-" + date.day;
          });
          plot.x = dateString;
          data.push(plot);
        });
        $scope.loading = false;
        //Inisialisasi plot
        Plotly.newPlot(target_test, data, layout, { responsive: true });
      },
      function myError(reason) {}
    );
  }
});

// Controller Chart Line Series
app.controller("AllDepositCtrl", function($scope, $http) {
  //Inisialisasi Data
  $scope.selectedValue = "";
  var depositArray = [];
  changeData();
  BindYears();
  //Mengganti Perubahan Pada Dropdown
  $scope.$watch("selectedValue", function() {
    changeData();
  });

  //Array nama tenant yang tersedia
  function BindYears() {
    $scope.YearList = ["tenant_test"];
  }

  //ID Chart pada HTML
  // Dokumentasi; https://plot.ly/javascript/
  var target_test = document.getElementById("deposit");
  var layout = {
    xaxis: {
      title: "Transaction Date"
    },

    yaxis: {
      title: "Amount"
    }
  };

  $scope.loading = true;
  function changeData() {
    var data = [];
    console.log("change data");

    //Fetch data dari backend
    $http({
      headers: {
        Authorization: "Basic cmVzdDpyZXN0"
      },
      method: "GET",
      datatype: "json",
      url:
        "http://propensi.cico.me:8080/tenancy-management/api/dashboard/deposito?name=" +
        $scope.selectedValue
    }).then(
      function mySuccess(response) {
        //Memasukkan tiap series ke dalam chart
        console.log(response.data);
        for (var i = 0; i < response.data.data[1].values.length; i++) {
          depositArray.push(response.data.data[1].values[i]);
        }

        // response.data.data.forEach(function(element) {
        var plot = {
          showlegend: true,
          mode: "lines",
          type: "scatter",
          name: "Deposit"
        };
        plot.y = depositArray;
        var dateString = response.data.date.map(function(date) {
          return date.year + "-" + date.month + "-" + date.day;
        });
        plot.x = dateString;
        data.push(plot);
        // });
        $scope.loading = false;
        //Inisialisasi plot
        Plotly.newPlot(target_test, data, layout, { responsive: true });
      },
      function myError(reason) {}
    );
  }
});
