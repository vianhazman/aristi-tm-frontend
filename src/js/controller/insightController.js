app.controller("insightCtrl", function($scope, $http) {
  $scope.init = function(name) {
    $scope.type = name;
  };
  $scope.remarkName = "";
  $scope.dateStart = "";
  $scope.dateStartParse = "";
  $scope.dateEnd = "";
  $scope.dateEndParse = "";
  $scope.$watch("dateStart", function() {
    $scope.dateStartParse = $scope.dateStart.toLocaleDateString();
    console.log($scope.dateStartParse);
  });
  $scope.$watch("dateEnd", function() {
    $scope.dateEndParse = $scope.dateEnd.toLocaleDateString();
    console.log($scope.dateEndParse);
  });

  $scope.getInsight = function() {
    //Fetch data dari backend
    $http({
      headers: {
        Authorization: "Basic cmVzdDpyZXN0"
      },
      method: "GET",
      datatype: "json",
      params: {
        tenantName: $scope.selectedValue,
        remarkName: $scope.remarkName,
        type: $scope.type,
        startDate: $scope.dateStartParse,
        endDate: $scope.dateEndParse
      },
      url:
        "http://propensi.cico.me:8080/tenancy-management/api/dashboard/getInsight"
    }).then(
      function mySuccess(response) {
        console.log(response.data);
        $scope.percentage = response.data.difference.differentPercent;
        $scope.minVal = response.data.summary.min;
        $scope.maxVal = response.data.summary.max;
        $scope.mean = response.data.summary.mean;
        $scope.median = response.data.summary.median;
        var dateString = response.data.difference.timestamp.map(function(date) {
          return date.year + "-" + date.month + "-" + date.day;
        });
        var target = document.getElementById("insightGraph");
        var plot = {
          showlegend: true,
          mode: "lines",
          type: "scatter",
          name: response.data.remarkName
        };

        plot.x = dateString;
        plot.y = response.data.difference.data;
        var a = [];
        var layout = {
          xaxis: {
            title: "Transaction Date"
          },

          yaxis: {
            title: "Amount"
          }
        };

        a.push(plot);
        Plotly.newPlot(target, a, layout, {
          responsive: true
        });

        $("#myModal").modal();
      },
      function myError(reason) {}
    );
  };
});
