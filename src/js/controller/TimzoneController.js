app.controller('listTimezoneCTRL', function($scope, $http, $window) {
	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/timezone/list',
	}).then(function mySuccess(response) {
		$scope.timezone = response.data;
	}, function myError(reason) {
		$scope.timezone = reason.statusText;
	});
});

app.controller('listTimezoneCtrl1', function($scope, $http, $window) {
	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/timezone/list',
	}).then(function mySuccess(response) {
		$scope.timezone = response.data;
		$scope.pageSize = 20;
		$scope.currentPage = 0;
		$scope.numberOfPages=function(){
		        return Math.ceil($scope.timezone.length/$scope.pageSize);
	    }

	}, function myError(reason) {
		$scope.timezone = reason.statusText;
	});
});

app.controller('editTimezoneCtrl', function($scope, $http, $window, $location) {
	var id= $location.search().id;
	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/timezone/'+id,
	}).then(function mySuccess(response) {
		$scope.timezone = response.data;
	}, function myError(reason) {
		$scope.timezone = reason.statusText;
	});
});