// Controller Chart Line Series
app.controller("TenantAccountCtrl", function($scope, $http) {
  //Inisialisasi Data
  $scope.selectedValue = "tenant_test";
  $scope.remarksList = ["Active", "Closed"];
  var totalArray = [];
  changeData();
  BindYears();
  //Mengganti Perubahan Pada Dropdown
  $scope.$watch("selectedValue", function() {
    changeData();
  });

  //Array nama tenant yang tersedia
  function BindYears() {
    $scope.YearList = ["tenant_test"];
  }

  //ID Chart pada HTML
  // Dokumentasi; https://plot.ly/javascript/
  var target_test = document.getElementById("account");
  var layout = {
    xaxis: {
      title: "Date"
    },

    yaxis: {
      title: "Amount"
    }
  };

  $scope.loading = true;
  function changeData() {
    var data = [];
    console.log("change data");

    //Fetch data dari backend
    $http({
      headers: {
        Authorization: "Basic cmVzdDpyZXN0"
      },
      method: "GET",
      datatype: "json",
      url:
        "http://propensi.cico.me:8080/tenancy-management/api/dashboard/customer?name=" +
        $scope.selectedValue
    }).then(
      function mySuccess(response) {
        //Memasukkan tiap series ke dalam chart
        console.log(response.data);
        for (var i = 0; i < response.data.data[0].values.length; i++) {
          totalArray.push(
            response.data.data[0].values[i] + response.data.data[1].values[i]
          );
        }
        var plot = {
          showlegend: true,
          mode: "lines",
          type: "scatter",
          name: "total account"
        };
        plot.y = totalArray;
        var dateString = response.data.date.map(function(date) {
          return date.year + "-" + date.month + "-" + date.day;
        });
        plot.x = dateString;
        data.push(plot);
        $scope.loading = false;
        //Inisialisasi plot
        Plotly.newPlot(target_test, data, layout, { responsive: true });
      },
      function myError(reason) {}
    );
  }
});

app.controller("AccountCtrl", function($scope, $http) {
  $scope.activeCount = "";
  $scope.closedCount = "";
  dataStatus();
  function dataStatus() {
    $http({
      headers: {
        Authorization: "Basic cmVzdDpyZXN0"
      },
      method: "GET",
      datatype: "json",
      url:
        "http://propensi.cico.me:8080/tenancy-management/api/dashboard/customer/current"
    }).then(function mySuccess(response) {
      $scope.activeCount =
        response.data.data.activeCount + response.data.data.closedCount;
      $scope.closedCount = response.data.data.closedCount;
    });
  }
});
