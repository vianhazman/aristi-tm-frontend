app.controller('cpRegistrationCtrl', function($window, $scope, $http, $location) {
	$scope.btnclicked=false;
	$scope.cid=$location.search().CustomerId; 
	$scope.isCPNameInvalid="";
	$scope.isCPPhoneInvalid="";
	$scope.isCPEmailInvalid="";

	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/customer/' + $scope.cid,
		
	}).then(function mySuccess(response) {
		$scope.customerInfo = response.data;
	}, function myError(reason) {
		$scope.customerInfo = reason.statusText;
	});
	$scope.isFormComplete = function () {
	    if($scope.registrationForm.cpname == null){
			$scope.isCPNameInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.cpphone == null){
			$scope.isCPPhoneInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.cpemail == null){
			$scope.isCPEmailInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else{
			$scope.btnclicked = true;
	    	$scope.createCustomer();
	    }
		
	}	
	$scope.createCustomer = function() {
		
		var customer = {};
		var custId = $scope.cid;
		
		customer.cpName = $scope.registrationForm.cpname;
		customer.cpPhone = $scope.registrationForm.cpphone;
		customer.cpEmail = $scope.registrationForm.cpemail;
		customer.active = true;

		$http({
			headers: {
				'Authorization': 'Basic cmVzdDpyZXN0',
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
				'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token, Authorization'
			},
			method: 'POST',
			dataType: 'json',
			data: customer,
			url: '	http://localhost:8080/tenancy-management/api/customer/createCustomerCp',
			params : {
				customerId : custId
			}
		}).then(function mySuccess(response) {
			window.alert("Contact Person added successfully!");
			$window.location.replace("#!/customers/info?id="+$scope.cid);
		}, function myError(reason) {
			
		});
	};
});