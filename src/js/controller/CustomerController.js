app.controller('listCustomerCtrl', function($scope, $http) { 
	$scope.customer={};
	$http({ 
		headers: { 
			'Authorization': 'Basic cmVzdDpyZXN0', 
		}, 
		method: 'GET', 
		url: 'http://localhost:8080/tenancy-management/api/customer/list?page=1&showPerPage=15'  
	}).then(function mySuccess(response) { 
		$scope.customers = response.data; 
		$scope.pageSize = 15;
		$scope.currentPage = 0;

	}, function myError(reason) { 
		$scope.customers = reason.statusText; 
	}); 

	$scope.nextPage = function (currentPage) {
		$http({ 
			headers: { 
				'Authorization': 'Basic cmVzdDpyZXN0', 
			}, 
			method: 'GET', 
			url: 'http://localhost:8080/tenancy-management/api/customer/list?page='+(currentPage+1)+'&showPerPage=15'  
		}).then(function mySuccess(response) { 
			$scope.customers = response.data;
				$http({ 
					headers: { 
						'Authorization': 'Basic cmVzdDpyZXN0', 
					}, 
					method: 'GET', 
					url: 'http://localhost:8080/tenancy-management/api/customer/list?page='+(currentPage+2)+'&showPerPage=15'  
				}).then(function mySuccess(response) { 
					if(response.data==''){
						$scope.nextButton=true;
					} 
				}, function myError(reason) { 
					$scope.customers = reason.statusText;
				}); 
		}, function myError(reason) { 
			$scope.customers = reason.statusText;
		}); 
	};
	$scope.previousPage = function (currentPage) {
		$http({ 
			headers: { 
				'Authorization': 'Basic cmVzdDpyZXN0', 
			}, 
			method: 'GET', 
			url: 'http://localhost:8080/tenancy-management/api/customer/list?page='+(currentPage+1)+'&showPerPage=15'  
		}).then(function mySuccess(response) { 
			$scope.customers = response.data;
			$scope.nextButton=false; 
		}, function myError(reason) { 
			$scope.customers = reason.statusText;

		}); 
	};

	
});
app.filter('startFrom', function() {
	return function(input, start) {
		if (!input || !input.length) { return; }{
			start = +start; 
			return input.slice(start);
		}
	}
});
app.controller('customerInfoCtrl', function($scope, $http,$location) {
	// console.log('aksjdhakjsdhakjsh');
	// console.log($routeParams.customerId);

	var customerId = $location.search().id; 

	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/customer/' + customerId,
		
	}).then(function mySuccess(response) {
		$scope.customerInfo = response.data;
	}, function myError(reason) {
		$scope.customerInfo = reason.statusText;
	});
});


app.controller('customerEditCtrl', function($scope, $http,$location,$window) {
	$scope.btnclicked=false;
	var customerId = $location.search().id; 
	
	$http({ 
		headers: { 
			'Authorization': 'Basic cmVzdDpyZXN0', 
		}, 
		method: 'GET', 
		dataType:'json',
		url: 'http://localhost:8080/tenancy-management/api/bank/list',
		
	}).then(function mySuccess(response) { 
		$scope.listBank = response.data; 
		
	}, function myError(reason) { 
		$scope.listBank = reason.statusText; 
	}); 

	$http({ 
		headers: { 
			'Authorization': 'Basic cmVzdDpyZXN0', 
		}, 
		method: 'GET', 
		dataType:'json',
		url: 'http://localhost:8080/tenancy-management/api/institution-type/list',
		
	}).then(function mySuccess(response) { 
		$scope.listType = response.data; 
		
	}, function myError(reason) { 
		$scope.listType = reason.statusText; 
	}); 

	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/customer/' + customerId,
		
	}).then(function mySuccess(response) {
		$scope.customerInfo = response.data;
	}, function myError(reason) {
		$scope.customerInfo = reason.statusText;
	});

	$scope.isFormComplete2 = function () {
		$scope.isNameInvalid="";
		$scope.isAddressInvalid="";
		$scope.isPhoneInvalid="";
		$scope.isEmailInvalid="";
		$scope.isTaxPayerInvalid="";
		$scope.isNotaryInvalid="";
		$scope.isDateInvalid="";
		$scope.isInstitutionInvalid="";
		$scope.isBankInvalid="";
		$scope.isBankNumberInvalid="";

		if($scope.customerInfo.name == null){
			$scope.isNameInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.customerInfo.address == null){
			$scope.isAddressInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.customerInfo.phone == null){
			$scope.isPhoneInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.customerInfo.email == null){
			$scope.isEmailInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.customerInfo.taxpayerNumber == null){
			$scope.isTaxPayerInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.customerInfo.notaryCompanyActNumber == null){
			$scope.isNotaryInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.customerInfo.dateOfEstablishment == null){
			$scope.isDateInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.customerInfo.institutionTypeId == null){
			$scope.isInstitutionInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.customerInfo.bankId == null){
			$scope.isBankInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.customerInfo.bankAccountNumber == null){
			$scope.isBankNumberInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
	    }else{
	    	$scope.btnclicked=true;
			$scope.editCustomer();
	    }
		
	}	

	$scope.editCustomer = function() {
		var customer = {};
		customer.id= customerId;
		customer.name = $scope.customerInfo.name;
		customer.address = $scope.customerInfo.address;
		customer.dateOfEstablishment = $scope.customerInfo.dateOfEstablishment;
		customer.notaryCompanyActNumber = $scope.customerInfo.notaryCompanyActNumber;
		customer.taxpayerNumber = $scope.customerInfo.taxpayerNumber;
		customer.email = $scope.customerInfo.email;
		customer.phone = $scope.customerInfo.phone;
		customer.institutionTypeId = $scope.customerInfo.institutionTypeId;
		customer.bankId = $scope.customerInfo.bankId;
		customer.bankAccountNumber = $scope.customerInfo.bankAccountNumber;
		customer.active = true;

		$http({
			headers: {
				'Authorization': 'Basic cmVzdDpyZXN0',
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
				'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token, Authorization'
			},
			method: 'POST',
			dataType: 'json',
			data: customer,
			url: '	http://localhost:8080/tenancy-management/api/customer/update'
			

		}).then(function mySuccess(response) {
			$scope.customerData=response.data;
			window.alert("Customer Updated successfully!");
			$window.location.replace("#!/customers");
		}, function myError(reason) {

		});
		};
});

app.controller('registrationCtrl', function($window, $scope, $http) {
	$scope.btnclicked=false;
	$http({ 
		headers: { 
			'Authorization': 'Basic cmVzdDpyZXN0', 
		}, 
		method: 'GET', 
		dataType:'json',
		url: 'http://localhost:8080/tenancy-management/api/institution-type/list',
		
	}).then(function mySuccess(response) { 
		$scope.listType = response.data; 
		
	}, function myError(reason) { 
		$scope.listType = reason.statusText; 
	}); 



	$http({ 
		headers: { 
			'Authorization': 'Basic cmVzdDpyZXN0', 
		}, 
		method: 'GET', 
		dataType:'json',
		url: 'http://localhost:8080/tenancy-management/api/bank/list',
		
	}).then(function mySuccess(response) { 
		$scope.listBank = response.data; 
		
	}, function myError(reason) { 
		$scope.listBank = reason.statusText; 
	}); 



	$scope.isFormComplete = function () {
		$scope.isNameInvalid="";
		$scope.isAddressInvalid="";
		$scope.isPhoneInvalid="";
		$scope.isEmailInvalid="";
		$scope.isTaxPayerInvalid="";
		$scope.isNotaryInvalid="";
		$scope.isDateInvalid="";
		$scope.isInstitutionInvalid="";
		$scope.isBankInvalid="";
		$scope.isBankNumberInvalid="";

		if($scope.registrationForm.name == null){
			$scope.isNameInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.address == null){
			$scope.isAddressInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.phone == null){
			$scope.isPhoneInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.email == null){
			$scope.isEmailInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.taxpayer == null){
			$scope.isTaxPayerInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.notary == null){
			$scope.isNotaryInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.date == null){
			$scope.isDateInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.institution == null){
			$scope.isInstitutionInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.bank == null){
			$scope.isBankInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.registrationForm.banknumber == null){
			$scope.isBankNumberInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
	    }else{
	    	$scope.btnclicked=true;
	    	$scope.createCustomer();
	    }
		
	}	
	$scope.createCustomer = function() {
		var customer = {};

		customer.name = $scope.registrationForm.name;
		customer.address = $scope.registrationForm.address;
		customer.dateOfEstablishment = $scope.registrationForm.date;
		customer.notaryCompanyActNumber = $scope.registrationForm.notary;
		customer.taxpayerNumber = $scope.registrationForm.taxpayer;
		customer.email = $scope.registrationForm.email;
		customer.phone = $scope.registrationForm.phone;
		customer.institutionTypeId = $scope.registrationForm.institution;
		customer.bankId = $scope.registrationForm.bank;
		customer.bankAccountNumber = $scope.registrationForm.banknumber;
		customer.active = true;

		$http({
			headers: {
				'Authorization': 'Basic cmVzdDpyZXN0',
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
				'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token, Authorization'
			},
			method: 'POST',
			dataType: 'json',
			data: customer,
			url: '	http://localhost:8080/tenancy-management/api/customer/create'
			
		}).then(function mySuccess(response) {
			$scope.customerData=response.data;
			if ($scope.customerData.status=="success") {
				window.alert("Customer added successfully!");
				$window.location.replace("#!/customers/info?id="+$scope.customerData.data.id);
			}else{
				window.alert($scope.customerData.status);
				$scope.btnclicked=false;
			}
			
		}, function myError(reason) {

		});
	};
	 
});