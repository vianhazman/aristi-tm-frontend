app.controller('listAppCtrl', function($scope, $http) {
	//	$http.default.headers.common['Authorization'] = 'Basic cmVzdDpyZXN0';

	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0'
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/application/list'
	}).then(function mySuccess(response) {
		$scope.listApp = response.data;
		$scope.pageSize = 10;
		$scope.currentPage = 0;

		$scope.numberOfPages=function(){
		        return Math.ceil($scope.listApp.length/$scope.pageSize);
	    }
		
	}, function myError(reason) {
		$scope.listApp = reason.statusText;
	});
	
});



app.controller('detailAppId', function($scope, $http ,$location) { 
	var id= $location.search().id;
	$http({ 
		headers: { 
			'Authorization': 'Basic cmVzdDpyZXN0', 
		}, 
		method: 'GET', 
		dataType:'json',
		url: 'http://localhost:8080/tenancy-management/api/application/'+id,
		
	}).then(function mySuccess(response) { 
		$scope.apps = response.data; 
		$scope.apps = response.data;
	}, function myError(reason) { 
		$scope.apps = reason.statusText; 
	}); 
});

app.controller('AddAppCTRL', function($scope, $http, $window) {
	// console.log('aksjdhakjsdhakjsh');
	// console.log($routeParams.customerId);
	$scope.btnclicked=false;
	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/timezone/list',
	}).then(function mySuccess(response) {
		$scope.timezone = response.data;
	}, function myError(reason) {
		$scope.timezone = reason.statusText;
	});
	$scope.validation = function(){
		$scope.isNameInvalid = "";
		$scope.isTempInvalid = "";
		$scope.isTempIdInvalid ="";
		$scope.isTimezoneInvalid = "";
		if($scope.appName == null){
			$scope.isNameInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.templateName==null){
			$scope.isTempInvalid = "is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.defaultTemplateId==null){
			$scope.isTempIdInvalid="is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else if($scope.timezoneId==null){
			$scope.isTimezoneInvalid="is-invalid";
			window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
		}else{
			$scope.btnclicked=true;
			$scope.submitApp();
		}
		
	}	
	$scope.submitApp = function(){
		var app = {};
		app.appName = $scope.appName;
		app.templateName = $scope.templateName;
		app.defaultTemplateId = $scope.defaultTemplateId;
		app.timezoneId = $scope.timezoneId;
		app.appPackageList = [];
		app.usageParameterList = [];
		// window.alert(app.appName);
		$http({
			headers : {
				'Authorization' : 'Basic cmVzdDpyZXN0',
			},
			method : 'POST',
			dataType : 'json',
			data : app,
			url : 'http://localhost:8080/tenancy-management/api/application/create'
		}).then(function mySuccess(response) {
			
			$scope.appData = response.data; 
			window.alert("Application added successfully!")
			$window.location.replace("#!/application/info?id="+$scope.appData.id);
		}, function myError(reason) {
			window.alert(reason.statusText);
		});
	}
});