// Controller Chart Line Series
app.controller("TransactionCtrl", function($scope, $http) {
  //Inisialisasi Data
  $scope.selectedValue = "";
  $scope.remarksList = [];
  changeData();
  BindYears();
  //Mengganti Perubahan Pada Dropdown
  $scope.$watch("selectedValue", function() {
    changeData();
  });

  //Array nama tenant yang tersedia
  function BindYears() {
    $scope.YearList = ["tenant_test"];
  }

  //ID Chart pada HTML
  // Dokumentasi; https://plot.ly/javascript/
  var target_test = document.getElementById("transaction");
  var layout = {
    xaxis: {
      title: "Transaction Date"
    },

    yaxis: {
      title: "Amount"
    }
  };

  $scope.loading = true;
  function changeData() {
    var data = [];
    console.log("change data");

    //Fetch data dari backend
    $http({
      headers: {
        Authorization: "Basic cmVzdDpyZXN0"
      },
      method: "GET",
      datatype: "json",
      url:
        "http://propensi.cico.me:8080/tenancy-management/api/dashboard/transaksi?name=" +
        $scope.selectedValue
    }).then(
      function mySuccess(response) {
        //Memasukkan tiap series ke dalam chart
        response.data.data.forEach(function(element) {
          var plot = {
            showlegend: true,
            mode: "lines",
            type: "scatter",
            name: element.name
          };
          plot.y = element.values;
          var dateString = response.data.date.map(function(date) {
            return date.year + "-" + date.month + "-" + date.day;
          });
          plot.x = dateString;
          data.push(plot);
          $scope.remarksList.push(element.name);
        });
        $scope.loading = false;
        //Inisialisasi plot
        Plotly.newPlot(target_test, data, layout, { responsive: true });
      },
      function myError(reason) {}
    );
  }
});

app.controller("NowTransaksiCtrl", function($scope, $http) {
  $scope.sembilansatuCount = 0;
  $scope.tigaduaCount = 0;
  $scope.tigasatuCount = 0;
  $scope.tigapuluhCount = 0;
  $scope.sembilanpuluhCount = 0;
  $scope.sembilanduaCount = 0;
  $scope.otherCount = 0;
  $scope.delapanduaCount = 0;
  $scope.empatduaCount = 0;
  $scope.tigatigaCount = 0;
  $scope.duapuluhCount = 0;
  $scope.empatsatuCount = 0;
  $scope.empatpuluhCount = 0;
  var indexArray = [];
  dataTransaksi();
  function dataTransaksi() {
    $http({
      headers: {
        Authorization: "Basic cmVzdDpyZXN0"
      },
      method: "GET",
      datatype: "json",
      url:
        "http://propensi.cico.me:8080/tenancy-management/api/dashboard/transaksi?name=tenant_test"
    }).then(function mySuccess(response) {
      for (var i = 0; i < response.data.date.length; i++) {
        if (response.data.date[i].month == 4) {
          // indexArray.push(response.data.data.);
          $scope.sembilansatuCount += response.data.data[0].values[i];
          $scope.tigaduaCount += response.data.data[1].values[i];
          $scope.tigasatuCount += response.data.data[2].values[i];
          $scope.tigapuluhCount += response.data.data[3].values[i];
          $scope.sembilanpuluhCount += response.data.data[4].values[i];
          $scope.sembilanduaCount += response.data.data[5].values[i];
          $scope.otherCount += response.data.data[6].values[i];
          $scope.delapanduaCount += response.data.data[7].values[i];
          $scope.empatduaCount += response.data.data[8].values[i];
          $scope.tigatigaCount += response.data.data[9].values[i];
          $scope.duapuluhCount += response.data.data[10].values[i];
          $scope.empatsatuCount += response.data.data[11].values[i];
          $scope.empatpuluhCount += response.data.data[12].values[i];
        }
      }
      // for(var i=0; i<indexArray.length; i++){
      //   $scope.sembilansatuCount += response.data.data[0].values[indexArray[i]];
      //   $scope.tigaduaCount += response.data.data[1].values[indexArray[i]];
      //   $scope.tigasatuCount += response.data.data[2].values[indexArray[i]];
      //   $scope.tigapuluhCount += response.data.data[3].values[indexArray[i]];
      //   $scope.sembilanpuluhCount += response.data.data[4].values[indexArray[i]];
      //   $scope.sembilanduaCount += response.data.data[5].values[indexArray[i]];
      //   $scope.otherCount += response.data.data[6].values[indexArray[i]];
      //   $scope.delapanduaCount += response.data.data[7].values[indexArray[i]];
      //   $scope.empatduaCount += response.data.data[8].values[indexArray[i]];
      //   $scope.tigatigaCount += response.data.data[9].values[indexArray[i]];
      //   $scope.duapuluhCount += response.data.data[10].values[indexArray[i]];
      //   $scope.empatsatuCount += response.data.data[11].values[indexArray[i]];
      //   $scope.empatpuluhCount += response.data.data[12].values[indexArray[i]];
      // }
    });
  }
});
