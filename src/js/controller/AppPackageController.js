app.controller('packageCtrl', function($scope, $http,$location) {
	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/app-package/list',
	}).then(function mySuccess(response) {
		$scope.packageList = response.data;
	}, function myError(reason) {
		$scope.packageList = reason.statusText;
	});
});