app.controller("MainCtrl", function($scope) {
  $scope.reverseSort = false;
});

app.controller("listTenantCtrl", function($scope, $http) {
  $http({
    headers: {
      Authorization: "Basic cmVzdDpyZXN0"
    },
    method: "GET",
    url: "http://propensi.cico.me:8080/tenancy-management/api/template/list?"
  }).then(
    function mySuccess(response) {
      $scope.tenants = response.data.templates;
      $scope.pageSize = 10;
      $scope.currentPage = 0;
      $scope.numberOfPages = function() {
        return Math.ceil($scope.tenants.length / $scope.pageSize);
      };
    },
    function myError(reason) {
      $scope.tenants = reason.statusText;
    }
  );
});

app.controller("listTenantCtrlid", function($scope, $http, $location) {
  var id = $location.search().id;
  $http({
    headers: {
      Authorization: "Basic cmVzdDpyZXN0"
    },
    method: "GET",
    dataType: "json",
    url:
      "http://localhost:8080/tenancy-management/api/template/detail?templateId=" +
      id,
    param: {
      templateid: id
    }
  }).then(
    function mySuccess(response) {
      $scope.tenants = response.data;
      $scope.tenants = response.data;
    },
    function myError(reason) {
      $scope.tenants = reason.statusText;
      alert("myError");
    }
  );
});

app.controller("createTemplateController", function($scope, $http, $window) {
  $scope.btnclicked = false;
  $http({
    headers: {
      Authorization: "Basic cmVzdDpyZXN0"
    },
    method: "GET",
    dataType: "json",
    url: "http://localhost:8080/tenancy-management/api/application/list"
  }).then(
    function mySuccess(response) {
      $scope.listApps = response.data;
    },
    function myError(reason) {
      $scope.listApps = reason.statusText;
    }
  );

  $scope.isFormComplete = function() {
    // window.alert($scope.appId);
    $scope.isAppInvalid = "";
    $scope.isNameTypeInvalid = "";
    $scope.isIdentifierInvalid = "";
    $scope.isIPInvalid = "";
    $scope.isPortInvalid = "";
    $scope.isUsernameInvalid = "";
    $scope.isPasswordInvalid = "";

    if ($scope.appId == null) {
      $scope.isAppInvalid = "is-invalid";
      // window.alert($scope.isAppInvalid);
      window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
    } else if ($scope.nameType == null) {
      $scope.isNameTypeInvalid = "is-invalid";
      window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
    } else if ($scope.identifier == null) {
      $scope.isIdentifierInvalid = "is-invalid";
      window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
    } else if ($scope.ip == null) {
      $scope.isIPInvalid = "is-invalid";
      window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
    } else if ($scope.port == null) {
      $scope.isPortInvalid = "is-invalid";
      window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
    } else if ($scope.username == null) {
      $scope.isUsernameInvalid = "is-invalid";
      window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
    } else if ($scope.password == null) {
      $scope.isPasswordInvalid = "is-invalid";
      window.alert("LENGKAPI FORM SESUAI PETUNJUK !");
    } else {
      $scope.btnclicked = true;
      $scope.insertTemplate();
    }
  };

  $scope.insertTemplate = function() {
    var template = {};
    template.identifier = $scope.identifier;
    template.tenantTypeName = $scope.nameType;
    template.active = false;
    template.nameTemplate = $scope.nameTemplate;
    template.countryId = 9;
    template.reportId = 8;
    template.appId = $scope.appId;

    template.tenantIdentifier = "" + $scope.identifier + "9999";

    template.schemaServer = $scope.ip;
    template.schemaServerPort = $scope.port;
    template.schemaUsername = $scope.username;
    template.schemaPassword = $scope.password;
    template.schemaName = "mifostenant-" + $scope.identifier + "9999";
    template.autoUpdate = 1;

    $http({
      headers: {
        Authorization: "Basic cmVzdDpyZXN0"
      },

      method: "POST",
      url:
        "http://localhost:8080/tenancy-management/api/template-attribute/create",
      dataType: "json",
      data: template
    }).then(
      function mySuccess(response) {
        $scope.tenantTemplateData = response.data;
        if ($scope.tenantTemplateData.status == "success") {
          window.alert("Tenant template added successfully!");
          $window.location.replace(
            "#!/tenant-template/info?id=" + $scope.tenantTemplateData.data.id
          );
        } else {
          window.alert($scope.tenantTemplateData.status);
          $scope.btnclicked = false;
        }
      },
      function myError(reason) {}
    );
  };
});
