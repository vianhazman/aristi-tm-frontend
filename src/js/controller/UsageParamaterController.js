app.controller('usageCtrl', function($scope, $http,$location) {
	$http({
		headers : {
			'Authorization' : 'Basic cmVzdDpyZXN0',
		},
		method : 'GET',
		datatype : 'json',
		url : 'http://localhost:8080/tenancy-management/api/usage-parameter/list?appId=3',
	}).then(function mySuccess(response) {
		$scope.usageList = response.data;
	}, function myError(reason) {
		$scope.usageList = reason.statusText;
	});
});